import React, { useState } from "react";
import styles from "./Home.module.css";
import TodoItem from "./TodoItem/TodoItem";
import DefautlStubTask from "./TodoItem/DefaultStubTask/DefaultStubTask";
import AddTodo from "./AddTodo/AddTodo";

export const Home = (props) => {
  const [todos, setTodos] = useState(JSON.parse(localStorage.getItem("todos")));

  const generateId = () => {
    return "_" + Math.random().toString(36).substr(2, 9);
  };

  const addTodo = (title) => {
    const newTodoItem = {
      _id: generateId(),
      title: title,
      isCompleted: false,
    };
    const newTodos = [...todos, newTodoItem];
    setTodos(newTodos);
    localStorage.setItem("todos", JSON.stringify(newTodos));
  };

  const changeTodo = (todoID) => {
    const copy = [...todos];
    const current = copy.find((todo) => todo._id === todoID);
    current.isCompleted = !current.isCompleted;
    setTodos(copy);
    localStorage.setItem("todos", JSON.stringify(copy));
  };

  const removeTodo = (todoID) => {
    const changedTodos = [...todos.filter((todo) => todo._id !== todoID)];
    setTodos(changedTodos);
    localStorage.setItem("todos", JSON.stringify(changedTodos));
  };

  const getTodoItem = (todo) => (
    <TodoItem
      key={todo._id}
      todo={todo}
      changeTodo={changeTodo}
      removeTodo={removeTodo}
    />
  );

  const todosActive = todos
    .filter((todo) => (todo.isCompleted ? false : true))
    .map((todo) => getTodoItem(todo));

  const todosIsCompleted = todos
    .filter((todo) => (todo.isCompleted ? true : false))
    .map((todo) => getTodoItem(todo));

  return (
    <>
      <h1 className={styles.appTitle}>TODO</h1>
      <AddTodo addtodo={addTodo} />
      <div className={styles.wrapper}>
        <div className={styles.todosGroup}>
          <div className={styles.todosGroup__title}>Tasks</div>
          {todosActive.length !== 0 ? (
            todosActive
          ) : (
            <DefautlStubTask text={"Task list is empty"} />
          )}
        </div>

        <div className={styles.todosGroup}>
          <div className={styles.todosGroup__title}>Completed Tasks</div>
          {todosIsCompleted.length !== 0 ? (
            todosIsCompleted
          ) : (
            <DefautlStubTask text={"No completed tasks"} />
          )}
        </div>
      </div>
    </>
  );
};

export default Home;
