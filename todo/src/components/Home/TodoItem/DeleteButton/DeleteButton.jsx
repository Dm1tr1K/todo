import React from "react";
import styles from "./DeleteButton.module.css";
import { MdDelete } from "react-icons/md";

const DeleteButton = (props) => {
    return (
        <div className={styles.deleteButtonWparrer}>
            <MdDelete onClick={() => props.removeTodo(props.todo._id)} className={styles.deleteButton } size={24}/> 
      </div>
  );
};

export default DeleteButton;
