import React from "react";
import styles from "./DefaultStubTask.module.css";
import { BsEmojiDizzy } from "react-icons/bs";


const DefautlStubTask = (props) => {
    return (
      <div className={styles.defaultTaskContainer}>
        <BsEmojiDizzy size={28} />
        <div className={styles.text}>{props.text}</div>
        <BsEmojiDizzy size={28} />
      </div>
    );
}

export default DefautlStubTask;