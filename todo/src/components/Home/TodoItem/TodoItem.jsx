import React from "react";
import styles from "./TodoItem.module.css";
import CheckBox from "./CheckBox/CheckBox";
import cn from "classnames";
import DeleteButton from "./DeleteButton/DeleteButton";

const TodoItem = ({todo, changeTodo, removeTodo}) => {
  return (
    <div className={styles.todoItemContainer}>
      <div className={styles.todoItem__content}>
        <CheckBox changeTodo={changeTodo} todo={todo} />
        <div
          className={cn(styles.content, {
            [styles.contentActive]: todo.isCompleted,
          })}
        >
          {todo.title}
        </div>
      </div>
      <DeleteButton removeTodo={removeTodo} todo={todo} />
    </div>
  );
};

export default TodoItem;
