import React from "react";
import styles from "./CheckBox.module.css";
import { FaCheck } from "react-icons/fa";
import cn from "classnames";

const CheckBox = (props) => {
  return (
    <div
          className={cn(styles.checkBoxWrapper, {
          [styles.activeCheckBox]: props.todo.isCompleted
      }) }
      onClick={() => props.changeTodo(props.todo._id)}
    >
      {props.todo.isCompleted && (
        <FaCheck size={20} className={styles.checkBox} />
      )}
    </div>
  );
};

export default CheckBox;
