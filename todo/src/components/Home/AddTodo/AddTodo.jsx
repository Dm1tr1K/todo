import React, { useState } from "react";
import styles from "./AddTodo.module.css";
import { BsFillPlusCircleFill } from "react-icons/bs";

const AddTodo = (props) => {
  const [text, setText] = useState("");

  const addTask = (text) => {
    props.addtodo(text);
    setText("");
  };

  return (
    <div className={styles.addTodoContainer}>
      <div className={styles.title}>Add task</div>
      <div className={styles.addTodoInputContainer}>
        <input
          type="text"
          value={text}
          className={styles.input}
          onChange={(e) => setText(e.target.value)}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              addTask(text);
            }
          }}
        />
        <BsFillPlusCircleFill
          onClick={() => addTask(text)}
          size={28}
          className={styles.addButton}
        />
      </div>
    </div>
  );
};

export default AddTodo;
